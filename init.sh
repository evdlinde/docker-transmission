eval $(cat env)

docker create \
  --name=transmission \
  --volumes-from transmission_data \
  -p $PORT:9091 \
  -e PGID=$GROUPID -e PUID=$USERID \
  linuxserver/transmission
