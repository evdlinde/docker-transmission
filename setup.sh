eval $(cat env)
docker create \
  --name=transmission_data \
  -v /etc/localtime:/etc/localtime:ro \
  -v transmission_volume:/config \
  -v transmission_volume:/downloads \
  -v transmission_volume:/watch \
  -e PGID=$GROUPID -e PUID=$USERID \
  linuxserver/transmission echo "linuxserver/transmission data only container"
